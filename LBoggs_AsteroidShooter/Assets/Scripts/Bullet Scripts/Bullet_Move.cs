﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Move : MonoBehaviour {

    public float moveSpeed; //designer-friendly float to hold bullet move speed

    private Transform tf; //var to hold object transform

	// Use this for initialization
	void Start () {

        //Assign variables
        tf = GetComponent<Transform>();

	}
	
	// Update is called once per frame
	void Update () {

        //Move in the direction the bullet is facing
        tf.position += (tf.right * moveSpeed * Time.deltaTime);
       


    }
}
