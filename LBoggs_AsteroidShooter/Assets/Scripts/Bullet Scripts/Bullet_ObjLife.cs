﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_ObjLife : MonoBehaviour {

    //Declare variables
    public float bulletLife; //designer friendly var to store number of seconds of bullet lifetime

	// Use this for initialization
	void Start () {

        //Destroy bullet by x seconds after created
        Destroy(this.gameObject, bulletLife);

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
