﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_DealDamage : MonoBehaviour {

    public GameObject explosionPrefab; //public var to store explosion prefab

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D other)
    {

        //If the object is not the laser field itself
        if (other.gameObject.tag != "Laser")
        {
            //If it was an enemy
            if (other.gameObject.tag == "Enemy")
            {
                //Increment score by object score value, update HUD
                GameManager.instance.score += other.gameObject.GetComponent<Score_Counter>().scoreValue;
                GameManager.instance.SetHUDText();
            }

            //Create explosion on this object, explosion rotation is irrelevant
            Instantiate(explosionPrefab, this.gameObject.transform.position, this.gameObject.transform.rotation);
                        
            //Destroy what collided with the bullet
            Destroy(other.gameObject);

            //Destroy the bullet
            Destroy(this.gameObject);
            
            //Debug
            Debug.Log("Player bullet killed " + other.gameObject.name);
        }

        
            
        
    }
}
