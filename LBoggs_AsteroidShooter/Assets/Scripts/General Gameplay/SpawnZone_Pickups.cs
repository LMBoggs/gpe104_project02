﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnZone_Pickups : MonoBehaviour {

    //Declare Variables
    public float spawnChancePercent; // designer friendly variable to store how often a spawn item may appear
    public GameObject pickupToSpawn; // designer friendly variable to store which pickup to spawn

    private Transform tf; // my transform
    private Vector3 randomVector; //Random point within the circle -- to be calculated
    private Vector3 spawnLocation; // calculated location based on spawn center and random number generator
    //private bool spawnActive; // flag to tell if this spawn zone already holds a spawn item, or it if is empty
    

	// Use this for initialization
	void Start () {

        //Assign variables
        tf = GetComponent<Transform>(); // store this transform as a variable
        //spawnActive = false; // spawn begins empty

        
        

    }
	
	// Update is called once per frame
	void Update () {
                
        //Gather random vector data
        randomVector = Random.insideUnitCircle; // Receive random location within circle and store as var on every frame
        randomVector *= 3; // Stretch the vector to that point so it is now (up to) 17 units away from the origin (the area of the circle)

        //Calculate random spawn point based on spawn zone center and random vector
        spawnLocation = tf.position + randomVector;

        //Decide on small percent chance whether or not to spawn a pickup
        //If random float between 0 and one is LESS than percent chance, spawn item
        if (Random.value < spawnChancePercent)
        {
            //Create spawn item
            Instantiate(pickupToSpawn, spawnLocation, pickupToSpawn.gameObject.transform.rotation);

            //Set SpawnActive bool to True
            //spawnActive = true;

            //Debug
            Debug.Log("Pickup Spawned!");
            
        }
        
    }
}


