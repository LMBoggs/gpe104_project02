﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup_ExtraLife : MonoBehaviour {

    //declare variables
    public GameObject explosionPrefab; //public var to hold explosion effect prefab
    
    //When Triggered
    private void OnTriggerEnter2D(Collider2D other)
    {
        //Triggered by Player
        if (other.gameObject.tag == "Player")
        {
            //Play Pickup sound
            GameManager.instance.pickupLife_Source.Play();

            //Increment player lives
            GameManager.instance.playerLives++;

            //Update HUD
            GameManager.instance.SetHUDText();

            //Debug
            Debug.Log("Player picked up Extra Life");
                        
        }

        //Triggered by non-player object
        else
        {
            //Create explosion on this object, explosion rotation is irrelevant
            Instantiate(explosionPrefab, this.gameObject.transform.position, this.gameObject.transform.rotation);

            //Debug
            Debug.Log("Extra Life destroyed by" + other.gameObject.name);
                        
        }

            
        //Destroy this object
        Destroy(this.gameObject);
    }
}
