﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    //DECLARE VARIABLES
    //-----------------------------
    //Game Manager variable
    public static GameManager instance; //singleton for this object
    private bool gameOver_bool; //private flag to recognize if the game is over

    //Player variables
    public GameObject player; //variable to hold reference to player
    public int MAX_LIVES; //designer friendly variable to store the player's max lives
    public int playerLives; //public variable to store the player's current number of lives throughout the game
    public int score = 0; //holds player's score for the entire game
    public GameObject explosionPrefab; // public variable to hold explosion prefab for player gameOver

    //Enemy variables
    public int MAX_ENEMIES; // designer friendly variable to store how many enemies may be active at a time
    public int activeEnemies; // public var to store how many enemy objects are active, not for designers
    public GameObject[] enemySpwnPts; //designer friendly array of spawn points in the world for enemies
    public GameObject[] enemyList; //designer friendly list of enemies to make available to spawn
    public List<GameObject> activeEnemyList; //public variable to store which enemies are active in the world

    //Audio variables
    public AudioSource levelMusic_Source; //store child object w/ audio component here
    public AudioClip levelMuisc_Clip; // designer friendly var to store level music clip

    public AudioSource lifeLost_Source; //store child object w/ audio component here
    public AudioClip lifeLost_Clip; // designer friendly var to store life lost audio clip

    public AudioSource pickupLife_Source; //store child object w/ audio component here
    public AudioClip pickupLife_Clip; // designer friendly var to store life lost audio clip

    public AudioSource gameOver_Source; //store child object w/ audio component here
    public AudioClip gameOver_Clip; // designer friendly var to store life lost audio clip

    //Text variables
    public Text HUD_Text;
    public GameObject GameOverDisplay; 



    //AWAKE
    //----------------------------------------
    void Awake()
    {
        //Initialize this Game Manager
        // As long as there is not an instance already set
        if (instance == null)
        {
            instance = this; // Store THIS instance of the class (component) in the instance variable
            DontDestroyOnLoad(gameObject); // Don't delete this object if we load a new scene
        }
        else
        {
            Destroy(this.gameObject); // There can only be one - this new object must die
            Debug.Log("Warning: A second game manager was detected and destroyed.");
        }

        //Assign Variables
        //-------------------------------------
        //override audio clip to what designer has set
        levelMusic_Source.clip = levelMuisc_Clip; 
        lifeLost_Source.clip = lifeLost_Clip; 
        gameOver_Source.clip = gameOver_Clip; 
        pickupLife_Source.clip = pickupLife_Clip;

        //Set Player to center of the world
        player.transform.position = Vector3.zero;

        //Set player lives to max lives
        playerLives = MAX_LIVES;

        //Set HUD Text
        SetHUDText();

        //Play level music
        levelMusic_Source.Play();
    }
    
    
    //UPDATE
    //------------------------------------------
    // Update is called once per frame
    void Update()
    {
        //Manage enemy count on the field
        //--------------------------------
        //if enemies are less than max, spawn a new enemy
        if (activeEnemies < MAX_ENEMIES)
        {
            Instantiate(enemyList[Random.Range(0, enemyList.Length)], //pick from the list of enemies
                enemySpwnPts[Random.Range(0, enemySpwnPts.Length)].transform.position, //create at spwn point position from list
                enemyList[Random.Range(0, enemyList.Length)].transform.rotation); //create rotation from enemy start rotation

            //Debug
            Debug.Log("New enemy object created");
        }

        //Check for Game Over
        //-----------------------
        //if Game Over, allow player to quit
        if (gameOver_bool == true)
        {
            //if player presses exit
            if (Input.GetKeyDown(KeyCode.Q))
            {
                //Debug
                Debug.Log("The player quit the game.");

                //Quit Application
                Application.Quit();
            }
        }


    }


    //LOSE LIFE
    //---------------------------------------
    //Player loses a life
    public void LoseLife()
    {
        //decrease player lives by 1
        playerLives--;

        //if the player has lives remaining
        if (playerLives > 0)
        {
            //play lifeLost audio clip
            lifeLost_Source.Play();
            
            //set the player to the center of the world
            player.transform.position = Vector3.zero;

            //Reset the player's rotation 
            player.transform.rotation = Quaternion.Euler(0, 0, 0);
            
            //Destroy active enemies in list
            foreach(GameObject enemy in activeEnemyList)
            {
                Destroy(enemy.gameObject);
            }
            //clear the list
            activeEnemyList.Clear();

            //Set HUD Text
            SetHUDText();

            //Debug
            Debug.Log("Player lost a life");
        }

        //player has no more lives -> Game Over
        else
        {
            GameOver();
        }
    }

    //SET HUD TEXT
    //-------------------------
    public void SetHUDText()
    {
        HUD_Text.text = "Lives: " + playerLives + "                    " + "Score: " + score;
    }

    //GAME OVER
    //------------------------
    void GameOver()
    {
        //Create explosion on player, explosion rotation is irrelevant
        Instantiate(explosionPrefab, player.transform.position, player.transform.rotation);

        //Set HUD Text
        SetHUDText();

        //Destroy player object
        Destroy(player.gameObject);

        //Play GameOver audio, stop level music
        gameOver_Source.Play();
        levelMusic_Source.Stop();

        //Enable GameOver Display
        GameOverDisplay.SetActive(true);

        //Destroy active enemies in list
        foreach (GameObject enemy in activeEnemyList)
        {
            Destroy(enemy.gameObject);
        }
        //clear the list
        activeEnemyList.Clear();

        //set gameOver flag to true
        gameOver_bool = true;

        //Debug
        Debug.Log("No lives remaining. Game Over.");


    }


}
