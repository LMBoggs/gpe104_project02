﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion_Explode : MonoBehaviour {

    //declare variables
    private Animator myAnimator; //public var to store animator for explosions
    private AudioSource myAudio; // public var to store my explosion audio
    public float lifespan; 

	// Use this for initialization
	void Start () {

        //load variables
        myAnimator = GetComponent<Animator>(); //load animator component into variable 
        myAudio = GetComponent<AudioSource>(); //load audio source component on this object into variable

        //Play sound on start
        myAudio.Play();

    }
	
	// Update is called once per frame
	void Update () {

        //play explosion animation
        myAnimator.Play("Explosion_Animation");

        

        //Destroy object
        Destroy(this.gameObject, lifespan);
    }
}
