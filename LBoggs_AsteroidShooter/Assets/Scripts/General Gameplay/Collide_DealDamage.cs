﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collide_DealDamage : MonoBehaviour {

    public GameObject explosionPrefab; //variable to hold explosion to play on collision
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //When an an object collides with this
    private void OnCollisionEnter2D(Collision2D other)
    {
        //if the object is the player, take a life
        if (other.gameObject.tag == "Player")
        {
            //player loses a life
            GameManager.instance.LoseLife();
                        
            //Debug
            Debug.Log(this.gameObject.name + " collided with player");
        }

        else
        {
            //Debug
            Debug.Log(this.gameObject.name + " collided with " + other.gameObject.name);
        }


        //Create explosion on this object, explosion rotation is irrelevant
        Instantiate(explosionPrefab, this.gameObject.transform.position, this.gameObject.transform.rotation);

        //Destroy this object
        Destroy(this.gameObject);
        
        

    }
}
