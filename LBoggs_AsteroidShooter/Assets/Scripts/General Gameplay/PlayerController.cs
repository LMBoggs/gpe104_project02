﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    //Priavate Variables
    private Transform tf; // Create a variable for our transform component
    
    //Public Variables
    public float turnSpeed; // Create a variable for the degrees we rotate in one frame draw (Designer-friendly)
    public float moveSpeed; // variable to store player's move speed forwards and backwards (Designer-friendly)
    public GameObject bulletPrefab; // designer-friendly var to store bullet prefab

    // Use this for initialization
    void Start () {

        //Assign Variables
        tf = GetComponent<Transform>(); // Load our transform component into our variable


    }
	
	// Update is called once per frame
	void Update () {

            

        // Rotation controls [LEFT / RIGHT]
        //----------------------------------
        if (Input.GetButton("Left Rotate"))
        {
            tf.Rotate(0, 0, turnSpeed);
        }

        if (Input.GetButton("Right Rotate"))
        {
            tf.Rotate(0, 0, -turnSpeed);
        }


        // Accelerate and Reverse [FORWARD / BACK]
        //--------------------------------------
        if (Input.GetButton("Accelerate"))
        {
            tf.position += (tf.right * moveSpeed * Time.deltaTime);
        }

        if (Input.GetButton("Reverse"))
        {
            tf.position += (-tf.right * moveSpeed * Time.deltaTime);
        }

        //Shoot Bullets
        //---------------------
        if (Input.GetButtonDown("Shoot"))
        {
            //Create bullet at left gun position and rotation
            Instantiate(bulletPrefab, transform.Find("Gun_L").position, transform.Find("Gun_L").rotation);

            //Create bullet at right gun position and rotation
            Instantiate(bulletPrefab, transform.Find("Gun_R").position, transform.Find("Gun_R").rotation);

        }

    }
}
