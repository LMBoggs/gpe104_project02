﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lasers_killBoundary : MonoBehaviour {

    public GameObject explosionPrefab; // public var to hold explosion prefab

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //When an object leaves the playspace trigger zone, destroy the object
    void OnTriggerExit2D(Collider2D other)
    {
        //If the object is the player
        if (other.gameObject.tag == "Player")
        {
            //Create explosion on player, explosion rotation is irrelevant
            Instantiate(explosionPrefab, GameManager.instance.player.gameObject.transform.position, this.gameObject.transform.rotation);

            //player loses a life
            GameManager.instance.LoseLife();

            //Debug
            Debug.Log("Player hit the laser fence");
        }

        //The object is not a player

        else
        {
            //Destroy the object
            Destroy(other.gameObject);

            //Debug
            Debug.Log(other.gameObject.name + " was destroyed by laser fence");
        }
        
    }
}
