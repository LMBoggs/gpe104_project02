﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidRotator : MonoBehaviour {

    public float rotationSpeed; //designer friendly variable for rotation speed
    private Transform tf; //private transform variable

	// Use this for initialization
	void Start () {

        //Set variables
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {

        //Rotate asteroid on every frame
        tf.Rotate(0, 0, rotationSpeed);


	}
}
