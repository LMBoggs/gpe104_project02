﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid_Move : MonoBehaviour {

    //Declare variables

    public float speed; //designer friendly var to hold move speed of asteroid

    private Transform tf; // var to hold my transform
    private Vector3 myStart; // var to hold start position vector of asteroid
    private Vector3 playerStart; // var to hold position of player (destination)
    private Vector3 directionVector; // var to hold vector, calculate direction to travel
    private Vector3 moveVector; // var to hold and calculate direction and speed for movement

	// Use this for initialization
	void Start () {

        //if the player can be referenced
        if (GameManager.instance.player != null)
        {
            //Assign variables
            tf = GetComponent<Transform>(); // load my transform component into variable tf

            myStart = tf.position; // store my position on start
            playerStart = GameManager.instance.player.gameObject.transform.position; //store position of player

            directionVector = playerStart - myStart; //store diff between player and asteroid as direction
            directionVector.Normalize(); // normalize the vector to a magnitude of 1

            moveVector = directionVector * speed;
        }

	}
	
	// Update is called once per frame
	void Update () {

        //if the player can be referenced
        if (GameManager.instance.player != null)
        {
            //Move towards the point where the player was on start
            tf.position += moveVector; //move in that direction each frame at given speed
        }
            


	}
}