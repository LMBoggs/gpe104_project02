﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShip_SpawnRotation : MonoBehaviour {

    //Declare variables

    private Transform tf; // var to hold my transform

    private Vector3 myStartPosition; // var to hold start position vector of asteroid
    private Vector3 playerStartPosition; // var to hold position of player (destination)
    private Vector3 directionVector; // var to hold vector, calculate direction to travel

    private Vector3 myStartRotation; // var to hold start rotation vector of asteroid
    private Vector3 playerStartRotation; // var to hold rotation of player (destination)
    private float angle; // var to hold float, calculate angle to travel
    Quaternion targetRotation; // var to hold and calculate rotation


    // Use this for initialization
    void Start () {

        //if the player can be referened
        if (GameManager.instance.player != null)
        {
            //set variables
            tf = GetComponent<Transform>();
            myStartPosition = tf.position; // store my position 
            playerStartPosition = GameManager.instance.player.gameObject.transform.position; //store position of player 

            directionVector = playerStartPosition - myStartPosition; //store diff between player and asteroid as direction on every frame
            directionVector.Normalize(); // normalize the vector to a magnitude of 1


            //ROTATE
            //--------------------------------
            //Update variables every frame

            angle = Mathf.Atan2(directionVector.y, directionVector.x) * Mathf.Rad2Deg; //calculate angle based on direction to player
            targetRotation = Quaternion.Euler(0, 0, angle); //update angle to rotate towards
            transform.rotation = targetRotation;
        }

        
    }
	

}
