﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Active_Enemy_Counter : MonoBehaviour {

	// Use this for initialization
	void Start () {

        //increase static activeEnemy count
        GameManager.instance.activeEnemies++;
        GameManager.instance.activeEnemyList.Add(this.gameObject);


	}


    private void OnDestroy()
    {
        //decrease static acticeEnemy count
        GameManager.instance.activeEnemies--;
        GameManager.instance.activeEnemyList.Remove(this.gameObject);
    }

}
