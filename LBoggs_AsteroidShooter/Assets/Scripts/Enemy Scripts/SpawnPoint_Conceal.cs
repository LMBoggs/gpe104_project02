﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint_Conceal : MonoBehaviour {

    //Declare variables
    private SpriteRenderer myRenderer; // var to hold my sprite renderer


	// Use this for initialization
	void Start () {

        //set variables
        myRenderer = GetComponent<SpriteRenderer>();

        //disable renderer
        myRenderer.enabled = false;

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
